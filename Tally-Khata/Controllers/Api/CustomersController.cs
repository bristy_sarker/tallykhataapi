﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using Tally_Khata.Dto;
using Tally_Khata.Models;

namespace Tally_Khata.Controllers.Api
{
    public class CustomersController : ApiController
    {
        private ProjectDbContext db = new ProjectDbContext();

        // GET: api/Customers
        public IQueryable<Customer> GetCustomers()
        {
            return db.Customers;
        }



        public IHttpActionResult GetCustomerDetails(int id)
        {
            var customer = db.Customers.SingleOrDefault(x => x.CustomerId == id);
            if (customer == null)
            {
                return NotFound();
            }
            var dueSales = db.DueSaleses.Where(x => x.CustomerId == id).ToList();
            var dueList = dueSales.Select(dueSale => new DueSaleDto
            {
                CustomerId = dueSale.CustomerId,
                PayableAmount = dueSale.PayableAmount,
                ProbablePayableDate = dueSale.ProbablePayableDate,
                stockoutList = db.StockOutProducts.Where(x => x.StockOutProductsId == dueSale.StockOutProductsId).ToList()
            }).ToList();


            //foreach (var dueSale in dueSales)
            //{
            //    var dueSaleDto = new DueSaleDto
            //    {
            //        PayableAmount = dueSale.PayableAmount,
            //        ProbablePayableDate = dueSale.ProbablePayableDate,
            //        stockoutList = db.StockOutProducts.Where(x => x.StockOutProductsId == dueSale.StockOutProductsId).ToList()
            //    };
            //    dueList.Add(dueSaleDto);
            //}


            var customerDetails = new CustomerDetailsDto
            {
                DueAmount = customer.DueAmount,
                PhoneNo = customer.PhoneNo,
                CustomerName = customer.CustomerName,
                DueSalesList = dueList

            };
            return Ok(customerDetails);
        }

        // POST: api/Customers
        [ResponseType(typeof(Customer))]
        public IHttpActionResult PostCustomer(Customer customer)
        {
            var collection = new Dictionary<string, object>();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var isExists = CustomerPhoneNoExist(customer.PhoneNo);
                if (isExists)
                {
                    collection.Add("Message", "Customer Already Exists");
                }
                else
                {
                    customer.DueAmount = 0.0;
                    db.Customers.Add(customer);
                    db.SaveChanges();
                    collection.Add("SuccessMessage", "Customer Registration Successfully Done");
                }

            }
            catch (Exception e)
            {
                string message = e.Message;
                collection.Add("exceptionMessage", message);

            }
            return CreatedAtRoute("DefaultApi", new { id = customer.CustomerId }, collection);
        }

        private bool CustomerPhoneNoExist(string phoneNo)
        {
            var customer = db.Customers.ToList();
            var exist = customer.Any(x => x.PhoneNo == phoneNo);
            return exist;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}