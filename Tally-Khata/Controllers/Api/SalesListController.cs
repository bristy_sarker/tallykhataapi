﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Tally_Khata.Dto;
using Tally_Khata.Models;

namespace Tally_Khata.Controllers.Api
{
    public class SalesListController : ApiController
    {

        private ProjectDbContext db = new ProjectDbContext();

        [HttpGet]
        public List<SalesListDto> GetAllSalesList()
        {
            var allSoldItems = db.StockOutProducts.Select(x => x.ProductId).Distinct();
            List<SalesListDto> salesListDtos = new List<SalesListDto>();
            foreach (var allSoldItem in allSoldItems)
            {

                var salesInfo = GetParticularProductSalesInfo(allSoldItem);
                salesListDtos.Add(salesInfo);
            }

            return salesListDtos;
        }


        private SalesListDto GetParticularProductSalesInfo(int id)
        {
            var soldQuanity = db.StockOutProducts.Where(x => x.ProductId == id).Select(x => x.StockOutQuantity).ToList().Sum();
            var productName = db.Products.SingleOrDefault(x => x.ProductId == id).ProductName;
            var available = db.Products.SingleOrDefault(x => x.ProductId == id).AvailableQuantity;
            var salesList = new SalesListDto
            {
                ProductName = productName,
                SoldQuantity = soldQuanity,
                AvailableQuantity = available
            };
            return salesList;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
//ToList solves the issue of Decimal support issue in JetEntityFrameworkProvider. Total = storeDb.OF_Carts.Where(x => x.CartId == ShoppingCartId).ToList().Sum(t => t.Quantity * t.Item.UnitPrice);