﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Tally_Khata.Dto;
using Tally_Khata.Models;

namespace Tally_Khata.Controllers.Api
{
    public class ProductListAndDetailsController : ApiController
    {
        private ProjectDbContext db = new ProjectDbContext();
        [HttpGet]
        public List<ProductListDto> GetAllProductsList()
        {
            var productLists = db.Products.Where(x => x.IsActive == true);

            List<ProductListDto> productListDtos = new List<ProductListDto>();
            foreach (var productList in productLists)
            {
                ProductListDto productListDto = new ProductListDto
              {
                  ProductId = productList.ProductId,
                  ImageUrl = productList.ImageUrl,
                  AvailableQuantity = productList.AvailableQuantity,
                  ProductName = productList.ProductName

              };
                productListDtos.Add(productListDto);
            }


            return productListDtos;
        }
        [HttpGet]
        public IHttpActionResult GetProductDetails(int id)
        {
            var product = db.Products.SingleOrDefault(x => x.ProductId == id && x.IsActive == true);
            if (product == null)
            {
                return NotFound();
            }
            ProductDetailsDto productDetailsDto = new ProductDetailsDto
            {
                ProductId = product.ProductId,
                ImageUrl = product.ImageUrl,
                ProductName = product.ProductName,
                ReOrderLevel = product.ReOrderLevel,
                AvailableQuantity = product.AvailableQuantity,

                StockInDateTimes = db.StockInProducts.Where(c => c.ProductId == id).Select(a => a.StockInDate).ToList(),
                StockInQuantitys = db.StockInProducts.Where(c => c.ProductId == id).Select(a => a.StockInQuantity).ToList(),
                UnitPrices = db.StockInProducts.Where(c => c.ProductId == id).Select(a => a.StockInPrice).ToList()


            };


            return Ok(productDetailsDto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
