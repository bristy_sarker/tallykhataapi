﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Tally_Khata.Models;

namespace Tally_Khata.Controllers.Api
{
    public class ProductsController : ApiController
    {
        private ProjectDbContext db = new ProjectDbContext();


        [HttpGet]
        public IQueryable<Product> GetProducts()
        {
            var products = db.Products.Where(x => x.IsActive == true);
            return products;
        }


        [HttpGet]
        public IHttpActionResult GetProduct(int id)
        {
            var product = db.Products.SingleOrDefault(x => x.ProductId == id && x.IsActive == true);
            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        [HttpPut]
        public IHttpActionResult PutProduct(int id, Product product)
        {
            var collection = new Dictionary<string, object>();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var productInDb = db.Products.SingleOrDefault(c => c.ProductId == id && c.IsActive == true);
            if (productInDb == null)
            {
                return NotFound();
            }
            try
            {
                productInDb.ProductName = product.ProductName;
                productInDb.BarCodeNo = product.BarCodeNo;
                productInDb.Description = product.Description;
                productInDb.Color = product.Color;
                productInDb.ImageUrl = product.ImageUrl;
                productInDb.ReOrderLevel = product.ReOrderLevel;
                productInDb.SizeOrWeight = product.SizeOrWeight;
                db.SaveChanges();
                collection.Add("SuccessMessage", "Product Successfully Updated");

            }
            catch (Exception e)
            {

                string message = e.Message;
                collection.Add("exceptionMessage", message);
            }
            return CreatedAtRoute("DefaultApi", new { id = product.ProductId }, collection);
        }



        [HttpPost]
        public IHttpActionResult PostProduct(Product product)
        {
            var collection = new Dictionary<string, object>();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {

                var isExists = ProductExist(product.ProductName);
                if (isExists)
                {
                    collection.Add("Message", "Product already Exists");
                }
                else
                {
                    product.AvailableQuantity = 0;
                    product.IsActive = true;
                    db.Products.Add(product);
                    db.SaveChanges();
                    collection.Add("SuccessMessage", "Product Successfully Added");
                }


            }
            catch (Exception e)
            {

                string message = e.Message;
                collection.Add("exceptionMessage", message);

            }


            return CreatedAtRoute("DefaultApi", new { id = product.ProductId }, collection);

        }


        [HttpDelete]
        public IHttpActionResult DeleteProduct(int id)
        {
            var collection = new Dictionary<string, object>();
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return NotFound();
            }
            try
            {
                product.IsActive = false;
                db.SaveChanges();
                collection.Add("SuccessMessage", "Product Successfully Deleted");

            }
            catch (Exception e)
            {

                string message = e.Message;
                collection.Add("exceptionMessage", message);

            }
            return CreatedAtRoute("DefaultApi", new { id = product.ProductId }, collection);
        }



        private bool ProductExist(string productName)
        {
            var product = db.Products.ToList();
            var exist = product.Any(x => x.ProductName.ToUpper() == productName.ToUpper() && x.IsActive == true);
            return exist;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}