﻿using System.Linq;
using System.Web.Http;
using Tally_Khata.Dto;
using Tally_Khata.Models;

namespace Tally_Khata.Controllers.Api
{
    public class TotalReportController : ApiController
    {
        private ProjectDbContext db = new ProjectDbContext();

        [HttpGet]
        public IHttpActionResult GenerateTotalReport()
        {
            var totalInvest = db.StockInProducts.Sum(t => t.StockInQuantity * t.StockInPrice);
            var totalIncome = db.StockOutProducts.Sum(t => t.StockOutQuantity * t.StockOutPrice);
            var totalDue = db.Customers.Sum(t => t.DueAmount);

            var totalProfit = totalIncome - totalInvest;

            var report = new SalesReportDto
            {
                TotalInvestment = totalInvest,
                TotalIncome = totalIncome,
                TotalProfit = totalProfit,
                TotalDue = totalDue
            };
            return Ok(report);

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
