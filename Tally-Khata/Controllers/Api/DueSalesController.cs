﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Tally_Khata.Dto;
using Tally_Khata.Models;

namespace Tally_Khata.Controllers.Api
{
    public class DueSalesController : ApiController
    {
        private ProjectDbContext db = new ProjectDbContext();

        [HttpPost]
        public HttpResponseMessage PostStockOutProductsForDuePayment(DueSaleDto dueSaleDto)
        {


            var collection = new Dictionary<string, object>();
            var totalAmount = 0.0;
            //var currentDue = 0.0;
            var stockoutIds = new List<int>();
            if (dueSaleDto == null)
            {

                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            try
            {
                foreach (var stockout in dueSaleDto.stockoutList)
                {
                    var singleProduct = db.Products.SingleOrDefault(x => x.ProductId == stockout.ProductId);
                    if (singleProduct != null)
                    {
                        if (singleProduct.AvailableQuantity < stockout.StockOutQuantity)
                        {
                            var productName =
                                db.Products
                                .SingleOrDefault(x => x.ProductId == stockout.ProductId)
                                .ProductName;
                            collection.Add("SuccessMessage", productName + " Out Of Stock..Can't be Sold");
                            break;
                        }
                        var productTotalCost = stockout.StockOutQuantity * stockout.StockOutPrice;
                        totalAmount = totalAmount + productTotalCost;
                        singleProduct.AvailableQuantity = singleProduct.AvailableQuantity - stockout.StockOutQuantity;
                        db.Products.AddOrUpdate(singleProduct);
                        db.SaveChanges();
                    }
                    var sales = new StockOutProducts
                    {
                        ProductId = stockout.ProductId,
                        StockOutQuantity = stockout.StockOutQuantity,
                        StockOutPrice = stockout.StockOutPrice,
                        StockOutDate = DateTime.Now

                    };
                    db.StockOutProducts.Add(sales);
                    db.SaveChanges();
                    stockoutIds.Add(sales.StockOutProductsId);

                }
                var currentDue = totalAmount - dueSaleDto.PayableAmount;
                var singleOrDefault = db.Customers.SingleOrDefault(x => x.CustomerId == dueSaleDto.CustomerId);
                if (singleOrDefault != null)
                {
                    var previousDueAmount = singleOrDefault.DueAmount;
                    singleOrDefault.DueAmount = previousDueAmount + currentDue;
                    db.Customers.AddOrUpdate(singleOrDefault);
                    db.SaveChanges();
                }

                foreach (var stockoutId in stockoutIds)
                {
                    var dueSales = new DueSales
                    {
                        StockOutProductsId = stockoutId,
                        CustomerId = dueSaleDto.CustomerId,
                        PayableAmount = dueSaleDto.PayableAmount,
                        ProbablePayableDate = dueSaleDto.ProbablePayableDate
                    };
                    db.DueSaleses.Add(dueSales);
                    db.SaveChanges();
                }



            }
            catch (Exception e)
            {
                string message = e.Message;
                collection.Add("exceptionMessage", message);
            }

            return Request.CreateResponse(HttpStatusCode.Created, collection);

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}