﻿using System.Linq;
using System.Web.Http;
using Tally_Khata.Models;

namespace Tally_Khata.Controllers.Api
{
    public class TopTenSoldProductController : ApiController
    {
        private ProjectDbContext db = new ProjectDbContext();

     
        [HttpGet]
        public IHttpActionResult FindTopTenSoldItem()
        {

            var topTensoldItem = db.StockOutProducts.GroupBy(x => x.ProductId).
                Select(
                t => new
                {
                    productId = t.Key,
                    soldQuantity = t.Sum(y => y.StockOutQuantity),
                    productName = db.Products.FirstOrDefault(x => x.ProductId == t.Key).ProductName,
                    productImage = db.Products.FirstOrDefault(x => x.ProductId == t.Key).ImageUrl

                }
                ).OrderByDescending(x => x.soldQuantity).Take(10);


            return Ok(topTensoldItem);
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
