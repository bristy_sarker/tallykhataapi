﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Http;
using Tally_Khata.Models;

namespace Tally_Khata.Controllers.Api
{
    public class StockInProductsController : ApiController
    {
        private ProjectDbContext db = new ProjectDbContext();

        [HttpPost]
        public IHttpActionResult PostStockInProduct(StockInProduct stockInProduct)
        {

            var collection = new Dictionary<string, object>();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var product = db.Products.SingleOrDefault(c => c.ProductId == stockInProduct.ProductId && c.IsActive == true);
                if (product != null)
                {
                    product.AvailableQuantity = product.AvailableQuantity + stockInProduct.StockInQuantity;
                    db.Products.AddOrUpdate(product);
                    var existProduct =
                        db.StockInProducts
                        .SingleOrDefault(x => x.ProductId == stockInProduct.ProductId
                        && x.StockInDate == stockInProduct.StockInDate);

                    if (existProduct != null)
                    {
                        existProduct.StockInQuantity = existProduct.StockInQuantity+stockInProduct.StockInQuantity;
                        existProduct.StockInPrice = stockInProduct.StockInPrice;
                        db.StockInProducts.AddOrUpdate(existProduct);
                        collection.Add("SuccessMessage", "Product Successfully Stock In!!");

                    }
                    else
                    {
                        db.StockInProducts.Add(stockInProduct);
                        collection.Add("SuccessMessage", "Product Successfully Stock In");
                    }
                }

                db.SaveChanges();

            }
            catch (Exception e)
            {
                string message = e.Message;
                collection.Add("exceptionMessage", message);
            }
            return CreatedAtRoute("DefaultApi", new { id = stockInProduct.StockInProductId }, collection);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}