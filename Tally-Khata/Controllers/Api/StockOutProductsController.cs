﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Http;
using Tally_Khata.Models;

namespace Tally_Khata.Controllers.Api
{
    public class StockOutProductsController : ApiController
    {
        private ProjectDbContext db = new ProjectDbContext();


        [HttpPost]
        public IHttpActionResult PostStockOutProducts(List<StockOutProducts> stockoutList)
        {
            var collection = new Dictionary<string, object>();
            var isStockOut = false;

            if (stockoutList == null)
            {

                return NotFound();
            }
            try
            {
                foreach (var stockout in stockoutList)
                {
                    var singleProduct = db.Products.SingleOrDefault(x => x.ProductId == stockout.ProductId);
                    if (singleProduct != null)
                    {
                        if (singleProduct.AvailableQuantity < stockout.StockOutQuantity)
                        {
                            var productName =
                                db.Products
                                .SingleOrDefault(x => x.ProductId == stockout.ProductId)
                                .ProductName;
                            isStockOut = true;
                            collection.Add("SuccessMessage", productName + " Out Of Stock..Can't be Sold");
                            break;
                        }
                        singleProduct.AvailableQuantity = singleProduct.AvailableQuantity - stockout.StockOutQuantity;
                        db.Products.AddOrUpdate(singleProduct);
                    }
                    var sales = new StockOutProducts
                    {
                        ProductId = stockout.ProductId,
                        StockOutQuantity = stockout.StockOutQuantity,
                        StockOutPrice = stockout.StockOutPrice,
                        StockOutDate = DateTime.Now

                    };
                    db.StockOutProducts.Add(sales);
                }
                if (isStockOut == false)
                {
                    var rowEffect = db.SaveChanges();
                    var noOfProduct = rowEffect / 2;
                    collection.Add("SuccessMessage", noOfProduct + " Product Sold");
                }

            }
            catch (Exception e)
            {
                string message = e.Message;
                collection.Add("exceptionMessage", message);
            }

            return Ok(collection);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}