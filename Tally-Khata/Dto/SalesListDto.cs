﻿
namespace Tally_Khata.Dto
{
    public class SalesListDto
    {
        public string ProductName { get; set; }
        public int SoldQuantity { get; set; }
        public int AvailableQuantity { get; set; }
    }
}