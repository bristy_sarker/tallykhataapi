﻿using System;
using System.Collections.Generic;
using Tally_Khata.Models;

namespace Tally_Khata.Dto
{
    public class DueSaleDto
    {
        public int CustomerId { get; set; }
        public double PayableAmount { get; set; }
        public DateTime ProbablePayableDate { get; set; }
        public List<StockOutProducts> stockoutList { get; set; }

    }
}