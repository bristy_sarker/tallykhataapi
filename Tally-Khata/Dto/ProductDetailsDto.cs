﻿
using System;
using System.Collections.Generic;

namespace Tally_Khata.Dto
{
    public class ProductDetailsDto
    {
        public int ProductId { get; set; }

        public string ImageUrl { get; set; }

        public string ProductName { get; set; }

        public int ReOrderLevel { get; set; }

        public int AvailableQuantity { get; set; }

        public ICollection<DateTime> StockInDateTimes { get; set; }

        public ICollection<int> StockInQuantitys { get; set; }

        public ICollection<int> UnitPrices { get; set; }

    }
}