﻿
namespace Tally_Khata.Dto
{
    public class SalesReportDto
    {
        public double TotalInvestment { get; set; }
        public double TotalIncome { get; set; }
        public double TotalProfit { get; set; }
        public double TotalDue { get; set; }


    }
}