﻿
using System.Collections.Generic;

namespace Tally_Khata.Dto
{
    public class CustomerDetailsDto
    {
        public string CustomerName { get; set; }
        public string PhoneNo { get; set; }
        public double DueAmount { get; set; }
        public List<DueSaleDto> DueSalesList { get; set; }
    }
}