﻿
namespace Tally_Khata.Dto
{
    public class ProductListDto
    {

        public int ProductId { get; set; }

        public string ImageUrl { get; set; }

        public string ProductName { get; set; }

        public int AvailableQuantity { get; set; }
    }
}