namespace Tally_Khata.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDue : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DueSales",
                c => new
                    {
                        DueSalesId = c.Int(nullable: false, identity: true),
                        CustomerId = c.Int(nullable: false),
                        PayableAmount = c.Double(nullable: false),
                        ProbablePayableDate = c.DateTime(nullable: false),
                        StockOutProductsId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DueSalesId)
                .ForeignKey("dbo.Customer", t => t.CustomerId)
                .ForeignKey("dbo.StockOutProducts", t => t.StockOutProductsId)
                .Index(t => t.CustomerId)
                .Index(t => t.StockOutProductsId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DueSales", "StockOutProductsId", "dbo.StockOutProducts");
            DropForeignKey("dbo.DueSales", "CustomerId", "dbo.Customer");
            DropIndex("dbo.DueSales", new[] { "StockOutProductsId" });
            DropIndex("dbo.DueSales", new[] { "CustomerId" });
            DropTable("dbo.DueSales");
        }
    }
}
