namespace Tally_Khata.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class aff : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DueSales", "CustomerId", "dbo.Customer");
            DropForeignKey("dbo.StockOutProducts", "DueSales_DueSalesId", "dbo.DueSales");
            DropIndex("dbo.DueSales", new[] { "CustomerId" });
            DropIndex("dbo.StockOutProducts", new[] { "DueSales_DueSalesId" });
            DropColumn("dbo.StockOutProducts", "DueSales_DueSalesId");
            DropTable("dbo.DueSales");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.DueSales",
                c => new
                    {
                        DueSalesId = c.Int(nullable: false, identity: true),
                        CustomerId = c.Int(nullable: false),
                        PayableAmount = c.Double(nullable: false),
                        ProbablePayableDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.DueSalesId);
            
            AddColumn("dbo.StockOutProducts", "DueSales_DueSalesId", c => c.Int());
            CreateIndex("dbo.StockOutProducts", "DueSales_DueSalesId");
            CreateIndex("dbo.DueSales", "CustomerId");
            AddForeignKey("dbo.StockOutProducts", "DueSales_DueSalesId", "dbo.DueSales", "DueSalesId");
            AddForeignKey("dbo.DueSales", "CustomerId", "dbo.Customer", "CustomerId");
        }
    }
}
