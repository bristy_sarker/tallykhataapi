﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Tally_Khata.Startup))]
namespace Tally_Khata
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
