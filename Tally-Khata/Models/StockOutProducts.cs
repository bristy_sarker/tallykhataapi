﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tally_Khata.Models
{
    public class StockOutProducts
    {
        [Key]
        public int StockOutProductsId { get; set; }

        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }

        public int StockOutQuantity { get; set; }

        public int StockOutPrice { get; set; }

        public DateTime StockOutDate { get; set; }

    }
}