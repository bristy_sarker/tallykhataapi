﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tally_Khata.Models
{
    public class StockInProduct
    {
        [Key]
        public int StockInProductId { get; set; }

        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }

        [Required(ErrorMessage = "Quantity must not be empty!")]
        [Range(0, int.MaxValue, ErrorMessage = "Quantity Must Be Positive")]
        public int StockInQuantity { get; set; }

        [Required(ErrorMessage = "Price field must not be empty!")]
        [Range(0, int.MaxValue, ErrorMessage = "Price Must Be Positive")]
        public int StockInPrice { get; set; }

        public DateTime StockInDate { get; set; }

    }
}