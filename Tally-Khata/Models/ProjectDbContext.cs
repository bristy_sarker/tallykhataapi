﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Tally_Khata.Models
{
    public class ProjectDbContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<StockInProduct> StockInProducts { get; set; }
        public DbSet<StockOutProducts> StockOutProducts { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<DueSales> DueSaleses { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
        }


    }
}