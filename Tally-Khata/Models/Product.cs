﻿using System.ComponentModel.DataAnnotations;

namespace Tally_Khata.Models
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "Product Name is Required")]
        public string ProductName { get; set; }

        public string BarCodeNo { get; set; }

        [Required(ErrorMessage = "ReOrder level is Required")]
        public int ReOrderLevel { get; set; }

        public string SizeOrWeight { get; set; }

        public string Color { get; set; }


        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public string ImageUrl { get; set; }

        public int AvailableQuantity { get; set; }

        public bool IsActive { get; set; }
    }
}