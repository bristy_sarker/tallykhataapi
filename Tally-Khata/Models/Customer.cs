﻿using System.ComponentModel.DataAnnotations;

namespace Tally_Khata.Models
{
    public class Customer
    {
        [Key]
        public int CustomerId { get; set; }

        [Required]
        public string PhoneNo { get; set; }

        [Required]
        public string CustomerName { get; set; }

        public string Address { get; set; }

        public double DueAmount { get; set; }


    }
}