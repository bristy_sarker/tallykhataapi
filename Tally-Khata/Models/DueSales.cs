﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tally_Khata.Models
{
    public class DueSales
    {
        [Key]
        public int DueSalesId { get; set; }

        public int CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }

        public double PayableAmount { get; set; }

        public DateTime ProbablePayableDate { get; set; }

        public int StockOutProductsId { get; set; }
        [ForeignKey("StockOutProductsId")]
        public virtual StockOutProducts StockOutProducts { get; set; }



    }
}